﻿using Asv.Mavlink;
using Mav2Influx.Services;
using Newtonsoft.Json;
using System.Collections.Concurrent;
using System.Diagnostics;

namespace Mav2Influx.Workers
{
    public class ReplayWriter : BackgroundService, IObserver<IPacketV2<IPayload>>
    {
        private readonly ILogger<ReplayWriter> _logger;
        private readonly MavlinkSimulatorObservable _mavlinkSimulatorObservable;
        private readonly string _sessionName;
        private readonly FileInfo _replayFile;
        private readonly Stopwatch _stopwatch;
        private readonly ConcurrentQueue<TimestampedMavlinkPacketV2> _mavlinkMessages;
        private readonly IConfiguration _configuration;
        private readonly IMavlinkClient _mavlinkClient;

        public ReplayWriter(ILogger<ReplayWriter> logger, IMavlinkClient mavlinkClient,  MavlinkSimulatorObservable mavlinkSimulatorObservable, IConfiguration configuration)
        {
            _logger = logger;
            _mavlinkSimulatorObservable = mavlinkSimulatorObservable;
            _sessionName = Guid.NewGuid().ToString();
            _replayFile = new FileInfo($"{_sessionName}.mavreplay");
            _stopwatch = new Stopwatch();
            _mavlinkMessages = new ConcurrentQueue<TimestampedMavlinkPacketV2>();
            _configuration = configuration;
            _mavlinkClient = mavlinkClient;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var replayWriterEnabled = _configuration.GetValue("replayWriterEnabled", false);

            if (!replayWriterEnabled)
            {
                _logger.LogInformation("ReplayWriter is disabled. Shutting down worker...");
                return;
            }

            _stopwatch.Start();
            _mavlinkSimulatorObservable.Subscribe(this);
            _mavlinkClient.MavlinkV2Connection.Subscribe(this);

            while (!stoppingToken.IsCancellationRequested)
            {
                var writeBuffer = new List<string>();
                while (_mavlinkMessages.TryDequeue(out var packet))
                {
                    writeBuffer.Add(JsonConvert.SerializeObject(packet, Formatting.None, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All }));
                }
                Console.WriteLine(writeBuffer.Count);
                System.IO.File.AppendAllLines(_replayFile.FullName, writeBuffer);

                await Task.Delay(1000, stoppingToken);
            }
        }

        public void OnNext(IPacketV2<IPayload> packet)
        {
            _logger.LogInformation("Packet received {name}", packet.Name);
            _mavlinkMessages.Enqueue(new TimestampedMavlinkPacketV2(packet, _stopwatch.Elapsed));
        }

        public void OnCompleted() { }
        public void OnError(Exception error) { }
    }

    public class TimestampedMavlinkPacketV2
    {
        public IPacketV2<IPayload> Packet { get; }
        public TimeSpan Timestamp { get; }

        public TimestampedMavlinkPacketV2(IPacketV2<IPayload> packet, TimeSpan timestamp)
        {
            Timestamp = timestamp;
            Packet = packet;
        }
    }
}
