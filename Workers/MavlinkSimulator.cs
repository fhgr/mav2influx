﻿using Asv.Mavlink;
using Asv.Mavlink.V2.Common;
using Asv.Mavlink.V2.Fhgr;
using Mav2Influx.Services;
using System.Security.Principal;

namespace Mav2Influx.Workers
{
    public class MavlinkSimulator : BackgroundService
    {
        private readonly ILogger<MavlinkSimulator> _logger;
        private readonly IConfiguration _configuration;
        private readonly MavlinkSimulatorObservable _mavlinkSimulatorObservable;
        private readonly IEnumerable<IMavlinkClient> _mavlinkClients;
        private Task WaypointSimulationTask = Task.CompletedTask;
        private Task WaypointSimulationTask2 = Task.CompletedTask;

        private long simulationCounter = 0;
        private readonly Random _random = new Random();
        private DateTime lastSendTime = DateTime.UtcNow;
        private readonly TimeSpan simulationInterval = TimeSpan.FromMilliseconds(100);

        private static (double lat, double lon) startPos = (46.856379678414214, 9.515976142760596);
        private (double lat, double lon) currentPos = startPos;
        private (double lat, double lon) currentPos2 = startPos;

        private readonly TimeSpan TimeToCompleteAllWaypoints = TimeSpan.FromSeconds(60);

        private readonly (double lat, double lon)[] waypoints = new[] {
            (46.8573725966383, 9.517839181354772),
            (46.85662815219683, 9.518753346456856),
            (46.85561392730763, 9.516868118009821),
            startPos
        };

        public MavlinkSimulator(ILogger<MavlinkSimulator> logger, IConfiguration configuration, MavlinkSimulatorObservable mavlinkSimulatorObservable, IEnumerable<IMavlinkClient> mavlinkClients)
        {
            _logger = logger;
            _configuration = configuration;
            _mavlinkSimulatorObservable = mavlinkSimulatorObservable;
            _mavlinkClients = mavlinkClients;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var packetSimulationEnabled = _configuration.GetValue("packetSimulationEnabled", false);

            if (!packetSimulationEnabled)
            {
                _logger.LogInformation("Packet Simulator is disabled. Shutting down worker...");
                return;
            }

            var vehicleIds = _mavlinkClients.Select(p => p.MavlinkV2Connection.DataStream.Name.Split(":")[1]);

            while (!stoppingToken.IsCancellationRequested)
            {
                if (WaypointSimulationTask2.IsCompleted)
                {
                    WaypointSimulationTask2 = Task.Run(async () =>
                    {
                        var vehicleId = vehicleIds.Skip(1).First();
                        await Task.Delay(_random.Next(8000, 14000));

                        currentPos2 = startPos;
                        var updateRateInSeconds = 0.1;
                        var timeForWaypointInSeconds = TimeToCompleteAllWaypoints.TotalSeconds / waypoints.Length;

                        foreach (var (lat, lon) in waypoints)
                        {
                            var deltaLat = lat - currentPos2.lat;
                            var deltaLon = lon - currentPos2.lon;
                            var latSpeed = deltaLat / timeForWaypointInSeconds;
                            var lonSpeed = deltaLon / timeForWaypointInSeconds;

                            for (int i = 0; i < timeForWaypointInSeconds * (1 / updateRateInSeconds); i++)
                            {
                                currentPos2.lat += latSpeed * updateRateInSeconds;
                                currentPos2.lon += lonSpeed * updateRateInSeconds;


                                var gpsRawIntPacket = new GpsRawIntPacket();
                                gpsRawIntPacket.Payload.Alt = 700;
                                gpsRawIntPacket.SystemId = 1;
                                gpsRawIntPacket.Payload.Lat = Convert.ToInt32(currentPos2.lat * 10000000);
                                gpsRawIntPacket.Payload.Lon = Convert.ToInt32(currentPos2.lon * 10000000);

                                var globalPosIntPacket = new GlobalPositionIntPacket();
                                globalPosIntPacket.SystemId = 1;
                                globalPosIntPacket.Payload.Alt = 705;
                                globalPosIntPacket.Payload.Lat = Convert.ToInt32(currentPos2.lat * 10000000);
                                globalPosIntPacket.Payload.Lon = Convert.ToInt32(currentPos2.lon * 10000000);

                                _mavlinkSimulatorObservable.Publish(vehicleId, gpsRawIntPacket);
                                _mavlinkSimulatorObservable.Publish(vehicleId, globalPosIntPacket);



                                await Task.Delay(TimeSpan.FromSeconds(updateRateInSeconds));
                            }
                        }
                    }, stoppingToken);
                }

                if (WaypointSimulationTask.IsCompleted)
                {
                    WaypointSimulationTask = Task.Run(async () =>
                    {
                        var vehicleId = vehicleIds.Skip(0).First();

                        currentPos = startPos;
                        var updateRateInSeconds = 0.1;
                        var timeForWaypointInSeconds = TimeToCompleteAllWaypoints.TotalSeconds / waypoints.Length;

                        foreach (var (lat, lon) in waypoints)
                        {
                            var deltaLat = lat - currentPos.lat;
                            var deltaLon = lon - currentPos.lon;
                            var latSpeed = deltaLat / timeForWaypointInSeconds;
                            var lonSpeed = deltaLon / timeForWaypointInSeconds;

                            for (int i = 0; i < timeForWaypointInSeconds * (1 / updateRateInSeconds); i++)
                            {
                                currentPos.lat += latSpeed * updateRateInSeconds;
                                currentPos.lon += lonSpeed * updateRateInSeconds;

                                var gpsRawIntPacket = new GpsRawIntPacket();
                                gpsRawIntPacket.Payload.Alt = 700;
                                gpsRawIntPacket.SystemId = 2;
                                gpsRawIntPacket.Payload.Lat = Convert.ToInt32(currentPos.lat * 10000000);
                                gpsRawIntPacket.Payload.Lon = Convert.ToInt32(currentPos.lon * 10000000);

                                var globalPosIntPacket = new GlobalPositionIntPacket();
                                globalPosIntPacket.SystemId = 2;
                                globalPosIntPacket.Payload.Alt = 705;
                                globalPosIntPacket.Payload.Lat = Convert.ToInt32(currentPos.lat * 10000000);
                                globalPosIntPacket.Payload.Lon = Convert.ToInt32(currentPos.lon * 10000000);

                                _mavlinkSimulatorObservable.Publish(vehicleId, gpsRawIntPacket);
                                _mavlinkSimulatorObservable.Publish(vehicleId, globalPosIntPacket);



                                await Task.Delay(TimeSpan.FromSeconds(updateRateInSeconds));
                            }
                        }
                    }, stoppingToken);
                }

                //// check interval
                //if (DateTime.UtcNow - lastSendTime > simulationInterval)
                //{
                //    simulationCounter++;
                //    if (simulationCounter % 10 == 0)
                //    {
                //        foreach (var vehicleId in vehicleIds.Skip(1).Take(1))
                //        {
                            

                //            var chemicalPacket = new ChemicalDetectorDataPacket();
                //            chemicalPacket.SystemId = 0;
                //            chemicalPacket.Payload.Concentration =_random.NextSingle() * 20f;
                //            chemicalPacket.Payload.Bars = Convert.ToUInt16(Math.Ceiling(chemicalPacket.Payload.Concentration / 2.5));
                //            chemicalPacket.Payload.AgentId = 1;
                //            chemicalPacket.Payload.HazardLevel = Convert.ToUInt16(Math.Floor(chemicalPacket.Payload.Concentration / 2.5));
                                                        
                //            _mavlinkSimulatorObservable.Publish(vehicleId, chemicalPacket);
                //        }

                //        foreach(var vehicleId in vehicleIds.Skip(0).Take(1))
                //        {
                //            //WIND_COV
                //            var windCovPacket = new WindCovPacket();
                //            windCovPacket.SystemId = 0;
                //            windCovPacket.Payload.WindX = _random.NextSingle();
                //            windCovPacket.Payload.WindY = _random.NextSingle();

                //            var distancePacket = new DistanceSensorPacket();
                //            distancePacket.SystemId = 0;
                //            distancePacket.Payload.CurrentDistance = Convert.ToUInt16(_random.Next(10, 25));

                //            _mavlinkSimulatorObservable.Publish(vehicleId, windCovPacket);
                //            _mavlinkSimulatorObservable.Publish(vehicleId, distancePacket);
                //        }
                //    }

                //    lastSendTime = DateTime.UtcNow;
                //}

                await Task.Delay(100, stoppingToken);
            }
        }
    }
}
