﻿using Asv.Mavlink;
using Mav2Influx.Services;
using MQTTnet;
using MQTTnet.Client;

namespace Mav2Influx.Workers
{
    public class MqttWriter : BackgroundService
    {
        private readonly IEnumerable<IMavlinkClient> _mavlinkClients;
        private readonly IConfiguration _configuration;
        private readonly ILogger<MqttWriter> _logger;
        private readonly IMqttClient _mqttClient;
        private readonly MavlinkSimulatorObservable _mavlinkSimulatorObservable;
        private readonly bool _serializeToJson = false;
        private readonly List<MqttObserver> _mqttObservers = new();

        public MqttWriter(IEnumerable<IMavlinkClient> mavlinkClients, IConfiguration configuration, ILogger<MqttWriter> logger, MavlinkSimulatorObservable mavlinkSimulatorObservable)
        {
            var mqttFactory = new MqttFactory();
            _mavlinkClients = mavlinkClients;
            _configuration = configuration;
            _logger = logger;
            _mqttClient = mqttFactory.CreateMqttClient();
            _mavlinkSimulatorObservable = mavlinkSimulatorObservable;
            _serializeToJson = configuration.GetValue("serializeToJson", false);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var mqttEnabled = _configuration.GetValue("mqttEnabled", true);

            if (!mqttEnabled)
            {
                _logger.LogInformation("MQTT is disabled. Shutting down worker...");
                return;
            }

            // MQTT Setup
            var mqttBrokerAddress = _configuration.GetValue("mqttBrokerAddress", "192.168.43.131");
            var mqttBrokerPort = _configuration.GetValue("mqttBrokerPort", 1883);
            var mqttClientOptions = new MqttClientOptionsBuilder().WithTcpServer(mqttBrokerAddress, mqttBrokerPort).WithTimeout(TimeSpan.FromSeconds(10)).Build();
            var isConnected = false;

            while (!isConnected)
            {
                _logger.LogInformation("Connecting to MQTT Broker '{address}:{port}'", mqttBrokerAddress, mqttBrokerPort);
                var connectionResult = await _mqttClient.ConnectAsync(mqttClientOptions, CancellationToken.None);
                var connectionResultCode = connectionResult.ResultCode;
                isConnected = connectionResultCode == MqttClientConnectResultCode.Success;

                if (isConnected)
                {
                    _logger.LogInformation("Connection to MQTT Broker established!");
                }
                else
                {
                    _logger.LogWarning("Connection to MQTT Broker failed with status {status}. Retrying in 1s..", connectionResultCode);
                    await Task.Delay(1000, stoppingToken);
                }
            }

            // MAVLink subscription
            foreach (var mavlinkClient in _mavlinkClients)
            {
                var connectionId = mavlinkClient.MavlinkV2Connection.DataStream.Name.Split(":")[1];
                var observer = new MqttObserver(connectionId, _serializeToJson, _mqttClient, _configuration);
                mavlinkClient.MavlinkV2Connection.Subscribe(observer);
                _mavlinkSimulatorObservable.Subscribe(observer);
                _mqttObservers.Add(observer);
            }

            while (!stoppingToken.IsCancellationRequested)
            {
                // do nothing in this loop, just keep worker alive for OnPacketReceived event
                await Task.Delay(1000, stoppingToken);
            }
        }
    }
}
