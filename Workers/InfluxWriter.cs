﻿using Asv.Mavlink;
using Asv.Mavlink.Client;
using InfluxDB.Client;
using InfluxDB.Client.Api.Domain;
using InfluxDB.Client.Writes;
using Mav2Influx.Services;
using System.Collections.Concurrent;
using System.Reflection;

namespace Mav2Influx.Workers
{
    public class InfluxWriter : BackgroundService
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<InfluxWriter> _logger;
        private readonly ConcurrentQueue<PointData> _pointDataWriteQueue;
        private readonly MavlinkSimulatorObservable _mavlinkSimulatorObservable;
        private readonly IEnumerable<IMavlinkClient> _mavlinkClients;
        private readonly List<InfluxObserver> _influxObservers = new();

        public InfluxWriter(IEnumerable<IMavlinkClient> mavlinkClients, IConfiguration configuration, ILogger<InfluxWriter> logger, MavlinkSimulatorObservable mavlinkSimulatorObservable)
        {
            _configuration = configuration;
            _logger = logger;
            _pointDataWriteQueue = new ConcurrentQueue<PointData>();
            _mavlinkSimulatorObservable = mavlinkSimulatorObservable;
            _mavlinkClients = mavlinkClients;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var influxEnabled = _configuration.GetValue("influxEnabled", true);

            if (!influxEnabled)
            {
                _logger.LogInformation("Influx is disabled. Shutting down worker...");
                return;
            }

            // InfluxDB Setup
            var influxConnectionString = _configuration.GetValue("influxConnectionString", "http://192.168.1.24:8086");
            var influxConnectionToken = _configuration.GetValue("influxConnectionToken", "g6Ra9iRmkL1307v36xtSEADVeenZETBduEaP6pe-cY-nKNBzd4U411Q7KmkSOWHSqR5rbHJTxNYJsZn3DQjJ9A==");
            var influxDefaultBucket = _configuration.GetValue("influxDefaultBucket", "mavlink");
            var influxOrganization = _configuration.GetValue("influxOrganization", "fhgr");
            using var influxClient = new InfluxDBClient(influxConnectionString, influxConnectionToken);
            var isConnected = false;

            while (!isConnected)
            {
                _logger.LogInformation("Connecting to InfluxDB '{influxConnectionString}'", influxConnectionString);
                isConnected = await influxClient.PingAsync();
                if (isConnected)
                {
                    _logger.LogInformation("Connection to InfluxDB established!");
                }
                else
                {
                    _logger.LogWarning("Connection to InfluxDB failed. Retrying in 1s..");
                    await Task.Delay(1000, stoppingToken);
                }
            }

            var influxWriteApi = influxClient.GetWriteApiAsync();
            
            // MAVLink subscription
            foreach(var mavlinkClient in _mavlinkClients)
            {
                var connectionId = mavlinkClient.MavlinkV2Connection.DataStream.Name.Split(":")[1];
                var observer = new InfluxObserver(connectionId, _pointDataWriteQueue);
                mavlinkClient.MavlinkV2Connection.Subscribe(observer);
                _mavlinkSimulatorObservable.Subscribe(observer);
                _influxObservers.Add(observer);
            }

            while (!stoppingToken.IsCancellationRequested)
            {
                if (_pointDataWriteQueue.Count < 500)
                {
                    await Task.Delay(TimeSpan.FromMilliseconds(500), stoppingToken);
                    continue;
                }

                var pointDataCollection = new List<PointData>();
                while (_pointDataWriteQueue.TryDequeue(out var point))
                {
                    pointDataCollection.Add(point);
                }

                await influxWriteApi.WritePointsAsync(pointDataCollection, influxDefaultBucket, influxOrganization, stoppingToken);
                _logger.LogInformation($"{pointDataCollection.Count} points written to influxdb");
            }
        }
    }
}
