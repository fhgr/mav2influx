﻿using Newtonsoft.Json;
using System.Diagnostics;

namespace Mav2Influx.Workers
{
    public class ReplayPlayer : BackgroundService
    {
        private readonly ILogger<ReplayPlayer> _logger;

        public ReplayPlayer(ILogger<ReplayPlayer> logger)
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogWarning("TODO. Replay Player currently disabled.");
            return;

            var file = new FileInfo("e5981b44-04b0-40e2-b6fd-64154d892e76.mavreplay");

            var packets = File.ReadAllLines(file.FullName)
                .Select(p => JsonConvert.DeserializeObject<TimestampedMavlinkPacketV2>(p, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All }))
                .ToList();

            var sw = Stopwatch.StartNew();

            foreach(var packet in packets)
            {
                var delta = packet.Timestamp - sw.Elapsed;
                if(delta.Ticks > 0)
                {
                    await Task.Delay(delta);
                }

                Console.WriteLine(packet.Packet.ToString());

            }


        }
    }
}
