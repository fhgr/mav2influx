﻿using Asv.Mavlink;
using System.Diagnostics;

namespace Mav2Influx.Services
{
    public class MavlinkSimulatorObservable : IObservable<IPacketV2<IPayload>>, IDisposable
    {
        private readonly List<INamedMavlinkObserver> _observers;

        public MavlinkSimulatorObservable()
        {
            _observers = new List<INamedMavlinkObserver>();
        }


        public IDisposable Subscribe(IObserver<IPacketV2<IPayload>> observer)
        {
            if(observer is not INamedMavlinkObserver namedObserver)
            {
                throw new NotSupportedException();
            }

            _observers.Add(namedObserver);
            return this;
        }

        public void Publish(string vehicleId, IPacketV2<IPayload> packet)
        {
            foreach(var observer in _observers.Where(p => p.VehicleId == vehicleId))
            {
                observer.OnNext(packet);
            }
        }

        public void Dispose()
        {
            Debugger.Break();
        }
    }
}
