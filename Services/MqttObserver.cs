﻿using Asv.Mavlink;
using Asv.Mavlink.V2.Common;
using MQTTnet;
using MQTTnet.Client;
using Newtonsoft.Json;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Mav2Influx.Services
{
    public class MqttObserver : INamedMavlinkObserver
    {
        private readonly string _vehicleId;
        private readonly bool _serializeToJson;
        private readonly IMqttClient _mqttClient;
        private readonly bool _packetSimulationEnabled;
        private readonly MqttApplicationMessageBuilder _mqttApplicationMessageBuilder;
        private readonly JsonSerializerOptions _jsonSerializerOptions;

        public MqttObserver(string vehicleId, bool serializeToJson, IMqttClient mqttClient, IConfiguration configuration)
        {
            _vehicleId = vehicleId;
            _mqttClient = mqttClient;
            _serializeToJson = serializeToJson;
            _packetSimulationEnabled = configuration.GetValue("packetSimulationEnabled", false);
            _mqttApplicationMessageBuilder = new MqttApplicationMessageBuilder();
            _jsonSerializerOptions = new JsonSerializerOptions
            {
                NumberHandling = JsonNumberHandling.AllowReadingFromString | JsonNumberHandling.AllowNamedFloatingPointLiterals
            };
        }

        public string VehicleId => _vehicleId;

        public void OnCompleted() { }

        public void OnError(Exception error) { }

        public async void OnNext(IPacketV2<IPayload> packet)
        {
            // ignore packets with gps Lat = 0 if simulation is active
            if (_packetSimulationEnabled && packet is GlobalPositionIntPacket globalPositionIntPacket && globalPositionIntPacket.Payload.Lat == 0)
            {
                return;
            }

            // ignore packets with gps Lat = 0 if simulation is active
            if (_packetSimulationEnabled && packet is GpsRawIntPacket gpsRawIntPacket && gpsRawIntPacket.Payload.Lat == 0)
            {
                return;
            }

            if (_serializeToJson)
            {
                var serializedPayload = System.Text.Json.JsonSerializer.SerializeToUtf8Bytes(packet.Payload, packet.Payload.GetType(), _jsonSerializerOptions);

                var applicationMessage = _mqttApplicationMessageBuilder
                    .WithTopic($"{_vehicleId}/{packet.Name}")
                    .WithPayload(serializedPayload)
                    .Build();

                await _mqttClient.PublishAsync(applicationMessage, CancellationToken.None);
            }
            else
            {
                foreach (var property in packet.Payload.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
                {
                    var rawValue = property.GetValue(packet.Payload);

                    string? value;
                    if (rawValue is float[] values)
                    {
                        value = $"[{string.Join(",", values)}]";
                    }
                    else
                    {
                        value = rawValue?.ToString();
                    }

                    var applicationMessage = new MqttApplicationMessageBuilder()
                         .WithTopic($"{_vehicleId}/{packet.Name}/{property.Name}")
                         .WithPayload(value)
                         .Build();

                    await _mqttClient.PublishAsync(applicationMessage, CancellationToken.None);
                }
            }
        }
    }
}
