﻿using Asv.Mavlink.V2.Common;
using Asv.Mavlink;
using MQTTnet.Client;
using MQTTnet;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using InfluxDB.Client.Writes;
using System.Collections.Concurrent;
using InfluxDB.Client.Api.Domain;

namespace Mav2Influx.Services
{
    public class InfluxObserver : INamedMavlinkObserver
    {
        private readonly string _vehicleId;
        private readonly ConcurrentQueue<PointData> _pointDataWriteQueue;

        public InfluxObserver(string vehicleId, ConcurrentQueue<PointData> pointDataWriteQueue)
        {
            _vehicleId = vehicleId;
            _pointDataWriteQueue = pointDataWriteQueue;
        }

        public string VehicleId => _vehicleId;

        public void OnCompleted() { }

        public void OnError(Exception error) { }

        public void OnNext(IPacketV2<IPayload> packet)
        {
            var pointBuilder = PointData.Builder.Measurement(packet.Name);
            pointBuilder.Timestamp(DateTime.UtcNow, WritePrecision.Ns);
            pointBuilder.Tag("SystemId", packet.SystemId.ToString());
            pointBuilder.Tag("ComponentId", packet.ComponenId.ToString());
            pointBuilder.Tag("VehicleId", _vehicleId);

            foreach (var property in packet.Payload.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                var rawValue = property.GetValue(packet.Payload);

                // check if value is convertable to float (int, short, double, float, bool, ...), otherwise skip field
                if (float.TryParse(property.GetValue(packet.Payload)?.ToString(), out var _))
                {
                    pointBuilder.Field(property.Name, rawValue);
                }
            }
            _pointDataWriteQueue.Enqueue(pointBuilder.ToPointData());
        }
    }
}
