﻿using Asv.Mavlink;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mav2Influx.Services
{
    public interface INamedMavlinkObserver : IObserver<IPacketV2<IPayload>>
    {
        string VehicleId { get; }
    }
}
