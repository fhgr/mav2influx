﻿// MIT License
//
// Copyright (c) 2018 https://github.com/asvol
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

// This code was generate by tool Asv.Mavlink.Shell version 1.0.0

using Asv.IO;

namespace Asv.Mavlink.V2.Fhgr
{

    public static class FhgrHelper
    {
        public static void RegisterFhgrDialect(this IPacketDecoder<IPacketV2<IPayload>> src)
        {
            src.Register(() => new RadiationDetectorDataPacket());
            src.Register(() => new ChemicalDetectorDataPacket());
        }
    }

    #region Enums

    /// <summary>
    ///  MAV_CMD
    /// </summary>
    public enum MavCmd : uint
    {
        /// <summary>
        /// Enable or disable an attached radiation detector
        /// Param 1 - enable (1.0) / disable (0.0) sensor
        /// Param 2 - Empty
        /// Param 3 - Empty
        /// Param 4 - Empty
        /// Param 5 - Empty
        /// Param 6 - Empty
        /// Param 7 - Empty
        /// MAV_CMD_TOGGLE_RADIATION_DETECTOR
        /// </summary>
        MavCmdToggleRadiationDetector = 40001,
    }


    #endregion

    #region Messages

    /// <summary>
    /// Radiation Detector Data
    ///  RADIATION_DETECTOR_DATA
    /// </summary>
    public class RadiationDetectorDataPacket : PacketV2<RadiationDetectorDataPayload>
    {
        public const int PacketMessageId = 201;
        public override int MessageId => PacketMessageId;
        public override byte GetCrcEtra() => 138;

        public override RadiationDetectorDataPayload Payload { get; } = new RadiationDetectorDataPayload();

        public override string Name => "RADIATION_DETECTOR_DATA";
    }

    /// <summary>
    ///  RADIATION_DETECTOR_DATA
    /// </summary>
    public class RadiationDetectorDataPayload : IPayload
    {
        public byte GetMaxByteSize() => 12; // Summ of byte sized of all fields (include extended)
        public byte GetMinByteSize() => 12; // of byte sized of fields (exclude extended)

        public void Deserialize(ref ReadOnlySpan<byte> buffer, int payloadSize)
        {
            var index = 0;
            var endIndex = payloadSize;
            var arraySize = 0;
            Timestamp = BinSerialize.ReadUInt(ref buffer);
            index += 4;
            SensorValue = BinSerialize.ReadUInt(ref buffer);
            index += 4;
            Factor = BinSerialize.ReadUInt(ref buffer);
            index += 4;

        }

        public int Serialize(ref Span<byte> buffer)
        {
            var index = 0;
            BinSerialize.WriteUInt(ref buffer, Timestamp);
            index += 4;
            BinSerialize.WriteUInt(ref buffer, SensorValue);
            index += 4;
            BinSerialize.WriteUInt(ref buffer, Factor);
            index += 4;
            return index; // /*PayloadByteSize*/12;
        }



        public void Deserialize(byte[] buffer, int offset, int payloadSize)
        {
            var index = offset;
            var endIndex = offset + payloadSize;
            var arraySize = 0;
            Timestamp = BitConverter.ToUInt32(buffer, index);
            index += 4;
            SensorValue = BitConverter.ToUInt32(buffer, index);
            index += 4;
            Factor = BitConverter.ToUInt32(buffer, index);
            index += 4;
        }

        public int Serialize(byte[] buffer, int index)
        {
            var start = index;
            BitConverter.GetBytes(Timestamp).CopyTo(buffer, index);
            index += 4;
            BitConverter.GetBytes(SensorValue).CopyTo(buffer, index);
            index += 4;
            BitConverter.GetBytes(Factor).CopyTo(buffer, index);
            index += 4;
            return index - start; // /*PayloadByteSize*/12;
        }


        public string Test { get; set; }

        /// <summary>
        /// Time since system boot
        /// OriginName: timestamp, Units: s, IsExtended: false
        /// </summary>
        public uint Timestamp { get; set; }
        /// <summary>
        /// Detector value scaled by factor
        /// OriginName: sensor_value, Units: n/a, IsExtended: false
        /// </summary>
        public uint SensorValue { get; set; }
        /// <summary>
        /// Scaling factor
        /// OriginName: factor, Units: none, IsExtended: false
        /// </summary>
        public uint Factor { get; set; }
    }
    /// <summary>
    /// Chemical Detector Data
    ///  CHEMICAL_DETECTOR_DATA
    /// </summary>
    public class ChemicalDetectorDataPacket : PacketV2<ChemicalDetectorDataPayload>
    {
        public const int PacketMessageId = 202;
        public override int MessageId => PacketMessageId;
        public override byte GetCrcEtra() => 211;

        public override ChemicalDetectorDataPayload Payload { get; } = new ChemicalDetectorDataPayload();

        public override string Name => "CHEMICAL_DETECTOR_DATA";
    }

    /// <summary>
    ///  CHEMICAL_DETECTOR_DATA
    /// </summary>
    public class ChemicalDetectorDataPayload : IPayload
    {
        public byte GetMaxByteSize() => 16; // Summ of byte sized of all fields (include extended)
        public byte GetMinByteSize() => 16; // of byte sized of fields (exclude extended)

        public void Deserialize(ref ReadOnlySpan<byte> buffer, int payloadSize)
        {
            var index = 0;
            var endIndex = payloadSize;
            var arraySize = 0;
            Concentration = BinSerialize.ReadFloat(ref buffer);
            index += 4;
            Dose = BinSerialize.ReadFloat(ref buffer);
            index += 4;
            AgentId = BinSerialize.ReadUShort(ref buffer);
            index += 2;
            Bars = BinSerialize.ReadUShort(ref buffer);
            index += 2;
            BarsPeak = BinSerialize.ReadUShort(ref buffer);
            index += 2;
            HazardLevel = BinSerialize.ReadUShort(ref buffer);
            index += 2;

        }

        public int Serialize(ref Span<byte> buffer)
        {
            var index = 0;
            BinSerialize.WriteFloat(ref buffer, Concentration);
            index += 4;
            BinSerialize.WriteFloat(ref buffer, Dose);
            index += 4;
            BinSerialize.WriteUShort(ref buffer, AgentId);
            index += 2;
            BinSerialize.WriteUShort(ref buffer, Bars);
            index += 2;
            BinSerialize.WriteUShort(ref buffer, BarsPeak);
            index += 2;
            BinSerialize.WriteUShort(ref buffer, HazardLevel);
            index += 2;
            return index; // /*PayloadByteSize*/16;
        }



        public void Deserialize(byte[] buffer, int offset, int payloadSize)
        {
            var index = offset;
            var endIndex = offset + payloadSize;
            var arraySize = 0;
            Concentration = BitConverter.ToSingle(buffer, index);
            index += 4;
            Dose = BitConverter.ToSingle(buffer, index);
            index += 4;
            AgentId = BitConverter.ToUInt16(buffer, index);
            index += 2;
            Bars = BitConverter.ToUInt16(buffer, index);
            index += 2;
            BarsPeak = BitConverter.ToUInt16(buffer, index);
            index += 2;
            HazardLevel = BitConverter.ToUInt16(buffer, index);
            index += 2;
        }

        public int Serialize(byte[] buffer, int index)
        {
            var start = index;
            BitConverter.GetBytes(Concentration).CopyTo(buffer, index);
            index += 4;
            BitConverter.GetBytes(Dose).CopyTo(buffer, index);
            index += 4;
            BitConverter.GetBytes(AgentId).CopyTo(buffer, index);
            index += 2;
            BitConverter.GetBytes(Bars).CopyTo(buffer, index);
            index += 2;
            BitConverter.GetBytes(BarsPeak).CopyTo(buffer, index);
            index += 2;
            BitConverter.GetBytes(HazardLevel).CopyTo(buffer, index);
            index += 2;
            return index - start; // /*PayloadByteSize*/16;
        }

        /// <summary>
        /// Concentration (mg/m3), IEEE floating point format
        /// OriginName: concentration, Units: mg/m3, IsExtended: false
        /// </summary>
        public float Concentration { get; set; }
        /// <summary>
        /// Dose (mg-min/m3), IEEE floating point format
        /// OriginName: dose, Units: mg-min/m3, IsExtended: false
        /// </summary>
        public float Dose { get; set; }
        /// <summary>
        /// Agent ID, 0=No agent, 1=GA, 2=GB, 3=GD/GF, 4=VX, 5=VXR, 6=DPM, 7=AC/CK, 8=CK, 9=AC, 11=HD
        /// OriginName: agent_id, Units: , IsExtended: false
        /// </summary>
        public ushort AgentId { get; set; }
        /// <summary>
        /// Bars (0 - 8)
        /// OriginName: bars, Units: , IsExtended: false
        /// </summary>
        public ushort Bars { get; set; }
        /// <summary>
        /// Peak Bars (0 - 8)
        /// OriginName: bars_peak, Units: , IsExtended: false
        /// </summary>
        public ushort BarsPeak { get; set; }
        /// <summary>
        /// Hazard Level - none, low, medium, high ( 0-3 )
        /// OriginName: hazard_level, Units: , IsExtended: false
        /// </summary>
        public ushort HazardLevel { get; set; }
    }


    #endregion


}
