# Mav2Influx
Converts mavlink messages to various other data structures (Influx data point, MQTT message, ...)

Currently implemented endpoints:
- InfluxDB
- MQTT

# Related Projects
- [Smiths Chemical Sensor Driver](https://gitlab.com/fhgr/smiths-chemical-sensor-driver)
- [Grafana-Influx-Mav2Influx Stack](https://gitlab.com/fhgr/grafana-influx-mav2influx)

# Todo
- [ ] Add documentation / readme

# Configuration
Either pass the following options as argument or define them as environment variables:

Features
- `--packetSimulationEnabled=false`
- `--replayWriterEnabled=false`
- `--influxEnabled=true`
- `--mqttEnabled=true`

Feature Config
- `--mqttBrokerAddress=127.0.0.1`
- `--mqttBrokerPort=1883`
- `--influxConnectionString=http://127.0.0.1:8086`
- `--influxConnectionToken=...`
- `--influxDefaultBucket=mavlink`
- `--influxOrganization=fhgr`
- `--mavlinkConnectionString=udp://0.0.0.0:14540`

### PacketSimulation
Simulates `GLOBAL_POS_INT` and `WIND_COV`

### ReplayWriter
Logs MAVLink Messages with correct timestamps to a `.mavreplay` file. One file per session. Can be used to replay the session in "real-time" (Correct time deltas)

### InfluxEnabled
If set, writes all incoming MAVLink messages to the InfluxDB

### MQTTEnabled
If set, publishes every field of every message to the configured MQTT Broker. Topic structure: `mavlink/{SystemId}/{ComponentId}/{MessageName}/{FieldName}`

See https://mavlink.io/en/messages/common.html

# Additional Notes
## MAVLink Connection
On Linux we must specifiy the "directed broadcast" IP-Address as MAVLink address. In most cases its `192.168.43.255` (Note: 255 at the end for broadcast). For more information see https://www.practicalnetworking.net/stand-alone/local-broadcast-vs-directed-broadcast/
On Windows we can additionally use `0.0.0.0` (`IPAddress.Any`) as valid option.