﻿using Asv.Mavlink;
using Asv.Mavlink.Client;
using Asv.Mavlink.V2.All;
using Asv.Mavlink.V2.Common;
using Asv.Mavlink.V2.Fhgr;
using Asv.Mavlink.V2.Standard;
using Mav2Influx.Services;
using Mav2Influx.Workers;
using Newtonsoft.Json;
using System.Reactive.Linq;

namespace Mav2Influx
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            Console.WriteLine($"Num Processor: {Environment.ProcessorCount}");
            var hostBuilder = Host.CreateDefaultBuilder(args);

            var configuration = new ConfigurationBuilder()
                .AddEnvironmentVariables()
                .AddCommandLine(args)
                .Build();

            hostBuilder.ConfigureAppConfiguration(config =>
            {
                config.AddEnvironmentVariables();
                config.AddCommandLine(args);
            });

            hostBuilder.ConfigureServices((builder, services) =>
            {
                var mavlinkConnectionStrings = configuration.GetValue("mavlinkConnectionStrings", "udp://0.0.0.0:14540").Split(";");

                foreach(var mavlinkConnectionString in mavlinkConnectionStrings)
                {
                    services.AddSingleton<IMavlinkClient>(p =>
                    {
                        var mavlinkV2Connection = new MavlinkV2Connection(mavlinkConnectionString, pd =>
                        {
                            pd.RegisterCommonDialect();
                            pd.RegisterStandardDialect();
                            pd.RegisterFhgrDialect();
                            pd.RegisterAllDialect();
                        });
                        return new MavlinkClient(mavlinkV2Connection, new MavlinkClientIdentity(), new MavlinkClientConfig());

                    });
                }

                services.AddSingleton<MavlinkSimulatorObservable>();
                services.AddHostedService<InfluxWriter>();
                services.AddHostedService<MqttWriter>();
                services.AddHostedService<MavlinkSimulator>();
                services.AddHostedService<ReplayWriter>();
                services.AddHostedService<ReplayPlayer>();
            });

            var host = hostBuilder.Build();
            await host.RunAsync();
        }
    }
}